class AddOrderDetailsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :coffee, :string, default: ''
    add_column :users, :coffee_size, :string, default: ''
    add_column :users, :coffee_delay, :integer, default: 0
    add_column :users, :coffee_extras, :string, default: ''

    add_column :users, :sandwich, :string, default: ''
    add_column :users, :sandwich_delay, :integer, default: 0
  end
end
