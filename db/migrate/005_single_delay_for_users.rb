class SingleDelayForUsers < ActiveRecord::Migration
  def change
    remove_column :users, :coffee_delay
    remove_column :users, :sandwich_delay

    add_column :users, :delay, :integer, default: 0, null: false
  end
end
