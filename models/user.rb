require 'active_record'
require 'aasm'
require 'i18n'

class User < ActiveRecord::Base
  # coffee, coffee_size, coffee_delay, coffee_extras,
  # sandwich, sandwich_delay,
  # pickup_time, total

  COFFEE_COOK_MINS = 2
  SANDWICH_COOK_MINS = 5

  include AASM

  aasm(:state) do
    state :new_customer, initial: true
    state :old_customer

    state :selected_coffee
    state :selected_coffee_size
    state :selected_coffee_extras

    state :selected_sandwich

    state :selected_delay
    state :checkout

    event :select_coffee do
      transitions to: :selected_coffee
    end
    event :select_coffee_size do
      transitions to: :selected_coffee_size
    end
    event :select_coffee_extras do
      transitions to: :selected_coffee_extras
    end
    event :select_sandwich do
      transitions to: :selected_sandwich
    end

    event :select_delay do
      transitions to: :selected_delay
    end
    event :complete do
      transitions to: :checkout
    end

    event :reset do
      transitions to: :old_customer
    end
  end

  def pickup_time
    Time.now + (delay + cook_time).minutes
  end

  def cook_time
    time = 0
    time += COFFEE_COOK_MINS if coffee
    time += SANDWICH_COOK_MINS if sandwich
    time
  end

  def start_time
    pickup_time - 5.minutes
  end

  def order_description
<<-TEXT
#{coffee_description}
#{sandwich_description}

Начните готовить заказ в #{start_time}
Стоимость заказа: #{order_summary}
TEXT
  end

  def coffee_description
<<-COFFEE
Пользователь заказал #{I18n.t("size.#{coffee_size}")} #{I18n.t("coffee.names.#{coffee}")}
Добавить #{I18n.t("coffee.extras.#{coffee_extras}")}
COFFEE
  end

  def sandwich_description
    "Cендвич #{I18n.t("sandwich.names.#{sandwich}")}" unless sandwich.empty?
  end

  def order_summary
    "Кофе #{coffee_price}р. + Сэндвич #{sandwich_price}р. = #{order_total}р."
  end

  def order_total
    coffee_price + sandwich_price
  end

  def coffee_price
    return 0 if coffee.empty?

    I18n.t("coffee.prices.#{coffee}.#{coffee_size}").to_i
  end

  def sandwich_price
    return 0 if sandwich.empty?

    I18n.t("sandwich.prices.#{sandwich}").to_i
  end
end
