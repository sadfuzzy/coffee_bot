require 'spec_helper'

describe MessageResponder do
  let(:user) { User.create(uid: 1, username: 'testuser') }
  let(:bot) { double('bot') }
  let(:chat) { double('chat') }
  let(:message) { double('message') }

  subject { MessageResponder.new(bot: bot, message: message, user: user) }

  before do
    allow(message).to receive(:text).and_return(command)
    allow(message).to receive(:chat).and_return(chat)
    allow(message).to receive(:from).and_return(user)

    allow(chat).to receive(:id).and_return(1)
    allow(chat).to receive(:username).and_return('andy')

    allow(bot).to receive_message_chain('api.send_message')
  end

  context 'when /start' do
    let(:command) { '/start' }
    it 'responds with coffee offer' do
      expect { subject.respond }.to change(user, :state).to('selected_coffee')
    end
  end

  context 'when Да' do
    let(:command) { 'Да' }
    it 'responds with coffee offer' do
      expect { subject.respond }.not_to change(user, :state)
    end
  end
end
