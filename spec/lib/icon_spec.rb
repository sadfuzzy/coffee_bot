require 'spec_helper'
require 'icon'

describe Icon do
  describe 'simple' do
    subject { Icon.new(name) }

    describe '#to_s' do
      let(:name) { 'back' }
      it { expect(subject.to_s).to eq('🔙') }
    end
  end
end
