require 'spec_helper'
require 'image_sender'

describe ImageSender do
  let(:image_name) { 'coffee/latte' }
  subject { ImageSender.new(bot: nil, image: image_name, chat: nil) }

  describe '#image' do
    it 'is a Faraday object' do
      expect(subject.image).to be_kind_of(Faraday::UploadIO)
    end

    it 'has correct path' do
      expect(subject.image.path).to include(image_name)
    end
  end
end
