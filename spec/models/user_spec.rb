require 'spec_helper'

describe User do
  let(:defaults) {
    {
      uid: 1,
      state: nil,
      coffee: nil,
      coffee_extras: nil,
      sandwich: nil,
      delay: 0 } }
  let(:attributes) { defaults }
  let(:user) { User.new(attributes) }

  context 'new_customer' do
    before { user.save }
    subject { user }

    it { is_expected.to allow_transition_to(:selected_coffee).on(:state) }
    it { is_expected.to allow_transition_to(:selected_sandwich).on(:state) }

    describe 'checkout' do
      specify 'linear' do
        expect {
          subject.select_coffee!
          subject.select_coffee_size!
          subject.select_coffee_extras!
          subject.select_sandwich!
          subject.select_delay!
          subject.complete!
          subject.reset!
        }.not_to raise_error
        expect(subject.state).to eq('old_customer')
      end

      specify 'non linear checkout' do
        expect {
          subject.select_coffee!
          subject.select_coffee_size!
          subject.select_coffee_extras!
          subject.select_sandwich!
          subject.select_delay!
          subject.complete!
          subject.reset!

          subject.select_coffee!
        }.not_to raise_error
        expect(subject.state).to eq('selected_coffee')
      end
    end
  end

  context 'existing user' do
    describe '#pickup_time' do
      let(:time) { Time.now }
      let(:delay) { 1 }
      before { user.save }

      subject { user.pickup_time }
      it { is_expected.not_to eq(time) }

      context 'with coffee' do
        let(:attributes) { defaults.merge({ delay: delay, coffee: 'latte' }) }

        it { is_expected.to be_within(1.minute).of(time + (delay + 2).minutes) }
      end

      context 'with sandwich' do
        let(:attributes) { defaults.merge({ delay: delay, sandwich: 'sandwich' }) }

        it { is_expected.to be_within(1.minute).of(time + (delay + 5).minutes) }
      end

      context 'with coffee & sandwich' do
        let(:attributes) { defaults.merge({ delay: delay, coffee: 'latte', sandwich: 'sandwich'}) }

        it { is_expected.to be_within(1.minute).of(time + (delay + 5 + 2).minutes) }
      end
    end

    describe '#pickup_time' do
      let(:time) { Time.now }
      let(:delay) { 1 }
      before { user.save }

      subject { user.pickup_time }
      it { is_expected.not_to eq(time) }

      context 'with coffee' do
        let(:attributes) { defaults.merge({ delay: delay, coffee: 'latte' }) }

        it { is_expected.to be_within(1.minute).of(time + (delay + 2).minutes) }
      end

      context 'with sandwich' do
        let(:attributes) { defaults.merge({ delay: delay, sandwich: 'sandwich' }) }

        it { is_expected.to be_within(1.minute).of(time + (delay + 5).minutes) }
      end

      context 'with coffee & sandwich' do
        let(:attributes) { defaults.merge({ delay: delay, coffee: 'latte', sandwich: 'sandwich'}) }

        it { is_expected.to be_within(1.minute).of(time + (delay + 5 + 2).minutes) }
      end
    end

    describe '#order_description' do
      let(:sandwich) { 'sandwich' }
      let(:attributes) { defaults.merge({
        coffee: 'latte',
        coffee_size: 'small',
        coffee_extras: 'both',
        sandwich: sandwich,
        delay: 0}) }
      before { user.save }
      let(:time) { user.start_time }
      subject { user.order_description }

      it { is_expected.to include 'Маленький Латте' }
      it { is_expected.to include 'Сахар' }
      it { is_expected.to include 'Корицу' }
      it { is_expected.to include "Начните готовить заказ в #{time}" }
      it { is_expected.to include 'Стоимость заказа:' }

      it { is_expected.to include user.coffee_price.to_s }
      it { is_expected.to include user.sandwich_price.to_s }
      it { is_expected.to include user.order_total.to_s }

      context 'without sandwich' do
        let(:sandwich) { '' }

        it { is_expected.not_to include 'Cендвич' }
      end
    end
  end
end
