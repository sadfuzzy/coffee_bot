require 'rubygems'
require 'bundler/setup'

require 'pg'
require 'active_record'
require 'yaml'

namespace :db do

  desc 'Migrate the APP_ENV database'
  task :migrate do
    environment = ENV['APP_ENV'] || 'development'
    connection_details = YAML::load(File.open('config/database.yml'))[environment]
    ActiveRecord::Base.establish_connection(connection_details)
    ActiveRecord::Migrator.migrate('db/migrate/')
  end

  desc 'Rollback the APP_ENV database by STEPS'
  task :rollback do
    environment = ENV['APP_ENV'] || 'development'
    steps = ENV['STEPS'] || 1
    connection_details = YAML::load(File.open('config/database.yml'))[environment]
    ActiveRecord::Base.establish_connection(connection_details)
    ActiveRecord::Migrator.rollback('db/migrate/', steps)
  end

  desc 'Create the APP_ENV database'
  task :create do
    environment = ENV['APP_ENV'] || 'development'
    connection_details = YAML::load(File.open('config/database.yml'))[environment]
    admin_connection = connection_details.merge({'database'=> 'postgres',
                                                'schema_search_path'=> 'public'})
    ActiveRecord::Base.establish_connection(admin_connection)
    ActiveRecord::Base.connection.create_database(connection_details.fetch('database'))
  end

  desc 'Drop the APP_ENV database'
  task :drop do
    environment = ENV['APP_ENV'] || 'development'
    connection_details = YAML::load(File.open('config/database.yml'))[environment]
    admin_connection = connection_details.merge({'database'=> 'postgres',
                                                'schema_search_path'=> 'public'})
    ActiveRecord::Base.establish_connection(admin_connection)
    ActiveRecord::Base.connection.drop_database(connection_details.fetch('database'))
  end
end
