class Icon
  attr_accessor :name

  ICON_TEXT = {
    cappuccino: '☕️',
    americano: '🇺🇸',
    latte: '🍦',
    sugar: '⬜️',
    cinnamon: '🍂',
    back: '🔙',
    chicken: '🐔',
    tuna: '🐟',
    sandwich: '🍔',
    burger: '🍔',

    smile: '😉',
    sad: '😒',
    yes: '✅',
    no: '❌',

    alarm: '⏰',
    min_0: '🕛',
    min_5: '🕐',
    min_10: '🕑',
    min_15: '🕒'
  }.freeze

  def initialize(name)
    @name = name
  end

  def to_s
    ICON_TEXT[name.to_sym]
  end
end
