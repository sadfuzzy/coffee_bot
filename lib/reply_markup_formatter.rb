class ReplyMarkupFormatter
  attr_reader :answers

  def initialize(answers)
    @answers = answers
  end

  def markup
    Telegram::Bot::Types::ReplyKeyboardMarkup
      .new(keyboard: answers,
           one_time_keyboard: true,
           force_reply: true,
           resize_keyboard: true)
  end
end
