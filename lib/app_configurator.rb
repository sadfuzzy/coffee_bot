require 'logger'
require 'pathname'

require './lib/database_connector'

class AppConfigurator
  def configure
    setup_i18n
    setup_database
  end

  def configure_test
    setup_i18n
    setup_database('test')
  end

  def token
    YAML.load(IO.read('config/secrets.yml'))['telegram_bot_token']
  end

  def logger
    Logger.new(STDOUT, Logger::DEBUG)
  end

  def root_path
    Pathname(__FILE__).dirname.parent
  end

  private

  def setup_i18n
    I18n.load_path = Dir['config/locales.yml']
    I18n.locale = :ru
    I18n.backend.load_translations
  end

  def setup_database(env = 'development')
    DatabaseConnector.establish_connection(env)
  end
end
