require './lib/bot'

class MessageResponder
  attr_reader :message, :bot, :user

  def initialize(options)
    @bot      = options[:bot]
    @message  = options[:message]
    @user     = options[:user]
  end

  def respond
    case message.text
    when %r{\/(start|new)}
      start_message
    when /#{Icon.new(:back)}/
      new_order_message
      offer_coffee
    when %r{^\/stop}
      stop_message
    when %r{^\/help}
      help_message
    when %r{^\/sandwich}
      ask_for_sandwich
    else
      non_command_answers
    end
  end

  def non_command_answers
    case user.state
    when 'new'
      offer_coffee
    when 'selected_coffee'
      ask_for_coffee_size
    when 'selected_coffee_size'
      ask_for_coffee_extras
    when 'selected_coffee_extras'
      ask_for_sandwich
    when 'selected_sandwich'
      ask_for_delay
    when 'selected_delay'
      checkout_message
    else
      unknown_message
    end
  end

  private

  def send_text(text, chat = message.chat)
    MessageSender.new(bot: bot, chat: chat, text: text).send
  end

  def notify_admins(text)
    AdminUser.all.map { |admin| send_text(text, admin) }
  end

  def send_image(image)
    ImageSender.new(bot: bot, chat: message.chat, image: image).send
  end

  def send_images(*images)
    images.map { |image| send_image(image) }
  end

  def send_menu(text, answers)
    MessageSender.new(bot: bot, chat: message.chat,
                      text: text, answers: answers).send
  end

  def footer_menu
    ["#{Icon.new(:back)} В начало"]
  end

  def yes_no_menu
    ["#{Icon.new(:yes)} Да", "#{Icon.new(:no)} Нет"]
  end

  def times_menu
    [
      ["#{Icon.new(:min_0)} Сейчас"],
      ["#{Icon.new(:min_5)} 5 мин"],
      ["#{Icon.new(:min_10)} 10 мин"],
      ["#{Icon.new(:min_15)} 15 мин"],
      footer_menu
    ]
  end

  def coffee_extras
    [
      [I18n.t('coffee.extras.sugar'), I18n.t('coffee.extras.cinnamon')],
      [I18n.t('coffee.extras.both')],
      [I18n.t('coffee.extras.none')],
      footer_menu
    ]
  end

  def sandwiches
    [
      [
        "#{I18n.t('sandwich.names.sandwich')} #{I18n.t('sandwich.prices.sandwich')}р.",
        "#{I18n.t('sandwich.names.burger')} #{I18n.t('sandwich.prices.burger')}р."],
      [
        "#{I18n.t('sandwich.names.chicken')} #{I18n.t('sandwich.prices.chicken')}р.",
        "#{I18n.t('sandwich.names.tuna')} #{I18n.t('sandwich.prices.tuna')}р."],
      [I18n.t('messages.no_sandwich')],
      footer_menu
    ]
  end

  def select_time(message_text)
    case message_text
    when /#{Icon.new(:min_5)}/
      5
    when /#{Icon.new(:min_10)}/
      10
    when /#{Icon.new(:min_15)}/
      15
    else
      0
    end
  end

  def select_coffe_and_price(message_text)
    case message_text
    when /#{Icon.new(:americano)}/
      { coffee: :americano,
        prices: ['210р – 200мл', '189р – 50мл'] }
    when /#{Icon.new(:cappuccino)}/
      { coffee: :cappuccino,
        prices: ['120р – 350мл', '120р – 200мл'] }
    else
      { coffee: :latte,
        prices: ['285р – 350мл', '265р – 200мл'] }
    end
  end

  def select_size(message_text, coffee)
    case message_text
    when /350/
      :standart
    when /200/
      coffee == 'americano' ? :standart : :small
    else
      :small
    end.to_s
  end

  def select_extras(message_text)
    case message_text
    when /и/
      :both
    when /#{Icon.new(:sugar)}/
      :sugar
    else
      :cinnamon
    end.to_s
  end

  def select_sandwich(message_text)
    case message_text
    when /#{Icon.new(:no)}/
      ''
    when /#{Icon.new(:burger)}/
      :burger
    when /#{Icon.new(:chicken)}/
      :chicken
    when /#{Icon.new(:tuna)}/
      :tuna
    else
      :sandwich
    end.to_s
  end

  def start_message
    notify_admins I18n.t('notifications.start', username: message.from.username)
    offer_coffee
  end

  def new_order_message
    notify_admins I18n.t('notifications.new', username: message.from.username)
  end

  def stop_message
    notify_admins I18n.t('notifications.stop', username: message.from.username)
    user.reset!
    send_text I18n.t('messages.farewell')
  end

  def help_message
    notify_admins I18n.t('notifications.help', username: message.from.username)
    send_text I18n.t('messages.help')
  end

  def unknown_message
    name = message.from.username
    text = message.text
    notify_admins I18n.t('notifications.unknown', username: name, message: text)
    send_text I18n.t('messages.unknown')
  end

  def offer_coffee
    send_menu('Чашку кофе?', [["#{Icon.new(:cappuccino)} Каппучино"],
                              ["#{Icon.new(:americano)} Американо"],
                              ["#{Icon.new(:latte)} Латте"],
                              footer_menu
                              ])
    # send_images 'coffee/all'

    user.select_coffee!
  end

  def ask_for_coffee_size
    coffee_prices = select_coffe_and_price(message.text)

    user.update_attributes coffee: coffee_prices[:coffee]

    send_menu('Выберите размер:', [%w(Стандартный Маленький),
                                   coffee_prices[:prices],
                                   footer_menu
                                  ])

    user.select_coffee_size!
  end

  def ask_for_coffee_extras
    user.update_attributes coffee_size: select_size(message.text, user.coffee)

    send_menu('Что добавить в кофе?', coffee_extras)
    # send_images 'coffee/extras'

    user.select_coffee_extras!
  end

  def ask_for_sandwich
    user.update_attributes coffee_extras: select_extras(message.text)

    send_menu("#{Icon.new(:sandwich)} А сэндвич хотите?", sandwiches)
    # send_images 'food/all'

    user.select_sandwich!
  end

  def ask_for_delay
    user.update_attributes sandwich: select_sandwich(message.text)

    send_menu(
      "#{Icon.new(:alarm)} Кофе готовится 2 мин. Когда заберёте?", times_menu)

    user.select_delay!
  end

  def checkout_message
    user.update_attributes delay: select_time(message.text)

    notify_admins I18n.t('notifications.text',
                          username: message.from.username,
                          text: user.order_description)

    time_text = user.pickup_time.strftime('%I:%M%p')
    send_menu I18n.t('messages.checkout', time: time_text), [footer_menu]

    user.complete!

    puts '<======== User ========>'
    puts user.inspect
  end
end
