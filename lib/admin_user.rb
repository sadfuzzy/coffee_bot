class AdminUser
  AU = Struct.new(:id, :username)

  class << self
    def all
      [AU.new(55_986_973, 'sadfuzzy'),
       AU.new(168_101_756, 'juuude')]
    end
  end
end
