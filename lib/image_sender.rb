require_relative './app_configurator'
require 'faraday'

class ImageSender
  IMAGES_FOLDER = "#{AppConfigurator.new.root_path}/assets/images".freeze

  attr_reader :bot, :image, :chat, :logger

  def initialize(options)
    @bot      = options[:bot]
    @image    = options[:image]
    @chat     = options[:chat]
    @logger = AppConfigurator.new.logger
  end

  def send
    bot.api.send_photo(chat_id: chat.id, photo: image)

    logger.debug "sending image '#{image.path}' to @#{chat.username}"
  end

  def image
    Faraday::UploadIO.new(image_path(@image), 'image/jpeg') if @image
  end

  private

  def image_path(image_name)
    "#{IMAGES_FOLDER}/#{image_name}.jpg"
  end
end
